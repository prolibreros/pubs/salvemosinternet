<section epub:type="chapter" role="doc-chapter">

# ¡Queremos redes libres!

Hace un tiempo, finalmente el gobierno de +++EE.UU.+++ eliminó
las protecciones que impedían a los operadores distinguir y priorizar
entre distintos tipos de tráfico en internet. Este fue el fin
anunciado de la neutralidad de red. Como consecuencia, los grandes
conglomerados que manejan las redes podrán discriminar el tráfico
y crear un equivalente a los «paquetes» de canales que ofrecen
los proveedores de +++TV+++ por cable. Algunos servicios comerciales
tendrán prioridad y serán más visibles, mientras que el acceso
libre a todo lo demás que exista en internet quedará relegado
a un segundo plano. _El peligro: la invisibilización y censura_
de facto _de medios alternativos y comunitarios_.

Pero ¿qué significa discriminar el tráfico y qué consecuencias
conlleva? Cuando Personal, Claro, Tuenti, Telcel, Movistar, +++AT&T+++,
etcétera te ofrecen «WhatsApp Messenger, Twitter e Instagram
gratis», están violando la neutralidad de red porque privilegian
el tráfico de un único servicio, mientras que alternativas más
seguras como Signal o Telegram quedan relegadas al paquete de
datos que forma parte del plan que contrataste. De esta manera,
las alternativas que quedan afuera de los paquetes promocionales
se vuelven más costosas y son relegadas a las personas convencidas
que tengan el dinero para pagar el plan más caro.

Entonces, la llamada _neutralidad de red es un principio fundamental
para garantizar el acceso universal a la información y el conocimiento_.

## La red nunca fue neutra

Sin embargo, las piratas creemos que la red nunca fue neutra,
por lo que el nombre puede resultar engañoso. El famoso principio
punta-a-punta de internet, donde cada computadora conectada a
la red puede acceder a cualquier otra computadora _y a su vez
puede ser accedida_ nunca se cumplió realmente.

Las configuraciones tecnopolíticas de los servicios de internet
domésticos impiden que las redes sean realmente libres y distribuidas.
Como ejemplos se tienen el ancho de banda asimétrico, donde podemos
bajar más datos de los que podemos subir; los puertos bloqueados,
que imposibilitan tener nuestros propios servidores de correo;
las +++IP+++ dinámicas, es decir, las direcciones de internet
que cambian todo el tiempo, por lo que impiden publicar servicios
---como plataformas _web_, nubes, radios, correos--- desde nuestras
casas sin depender de servidores externos.

En los últimos años también se ha sumado la inspección de paquetes.
Esta debilita nuestra capacidad de compartir archivos usando
redes +++P2P+++ y nos quita el control sobre los módem y ruteadores,
que ahora solo pueden configurarse a través del soporte del proveedor
de internet.

Se trata de un conjunto de medidas que han sido promovidas por
la _web_ 2.0. Bajo esta lógica todas las cosas que queremos publicar
y difundir, así como las conversaciones que queremos tener, pasan
y se alojan permanentemente en las atractivas infraestructuras
de servicios de terceros como son Twitter, Facebook, +++AWS+++
o Google Cloud.

Asimismo, gran parte de la infraestructura sobre la que se sostiene
internet ---el tendido de primera milla o los [cables interoceánicos](https://invidio.us/watch?v=QUmWOXRqJ_Y),
por ejemplo--- está en manos de un número cada vez más reducido
de corporaciones. La participación de los Estados es acotada
y, en muchos casos, se ve influenciada por el cabildeo de estas
empresas. Si la infraestructura de la que depende la red está
en manos de un oligopolio, no puede ser neutra ni, mucho menos,
ser controlada por sus usuarias. Recuerda qué pasó con el periódico,
la radio o la televisión, dejaron de ser medios de comunicación
para ser aparatos de propaganda masiva, ¿es eso lo que quieres
para internet?

Entonces, _para nosotras la «red de redes» nunca fue neutra,
sino que es política, jerárquica y centralizada_. La red de redes
tiene espesor, no es un espacio abstracto de dos dimensiones,
sino un lugar donde habitamos, crecemos, nos cuidamos y, principalmente,
luchamos como personas y comunidades dentro de los vericuetos
de esas murallas.

## Qué se hizo y qué hacemos

Aunque la red nunca fue neutra, siempre tuvimos formas de contrarrestar
las políticas jerarquizantes de la red y aun cuando estas estrategias
han estado relegadas a aquellas personas con conocimientos técnicos
o con fuerza de voluntad, vemos que aparecen cada vez más proyectos
que tienden a habilitar la publicación libre y distribuida antes
que centralizada.

De forma artesanal podemos configurar nuestros ruteadores domésticos
para que permitan conexiones entrantes o nieguen la entrada a
anuncios publicitarios. Poco a poco se puede habilitar una computadora
reciclada para que tenga nuestro sitio _web_ personal o de nuestra
comunidad.

Hace unos años se sumaron proyectos como [Freedombox](https://www.freedombox.org),
[Pi-hole](https://pi-hole.net) y similares, que permitían la
instalación y configuración de servidores domésticos para personas
que no tenían tiempo de cacharrear.

A la par, podemos compartir archivos a través de redes +++P2P+++
como torrents ---tan perseguidas por las discográficas y cinematográficas
y que todavía viven, más o menos invicta---. También se fueron
sumando proyectos que adaptan esos protocolos para que la publicación
pueda ser dinámica e incensurable. Dentro de esos proyectos podemos
mencionar a [Zeronet](https://zeronet.io/es), [+++IPFS+++](https://ipfs.io)
---utilizada por las piratas catalanas para difundir la convocatoria
al referéndum de independencia---, [+++DAT+++](https://www.datprotocol.com),
etcétera.

Pero las alternativas tecnológicas que están desarrollando varias
personas o comunidades no puede ir sola. Compas de México, Argentina,
Brasil, Uruguay, Colombia, Chile y más lugares hacen trabajo
pedagógico, como talleres y eventos, escriben documentación,
para que estas tecnologías sean más accesibles para todas, así
como se organizan para tener espacios físicos seguros para la
experimentación, el aprendizaje y el cuidado mutuo. Es decir,
_para hacer solo se requiere querer: querer colaborar, querer
aprender, querer escuchar, querer comprometerse y querer cuidar_.

![El esquema de red de Baran.](../img/img01.jpg)

## De la desobediencia tecnológica a las redes libres

Pero todos estos proyectos se montan sobre la infraestructura
física de internet. Ya vimos que los cables pertenecen a pocos
conglomerados y, para el caso de América Latina, están armados
de tal forma que todo nuestro tráfico global pasa necesariamente
por +++EE.UU.+++ En internet, como en la economía, la política
y la cultura, a nuestra región la quieren mantener dependiente
a través de intereses ajenos a nuestras necesidades.

Las redes libres y comunitarias son proyectos colectivos que
surgen de la necesidad local de tener acceso a internet ---o
del desafío técnico y político de tener una red autónoma--- en
aquellas zonas donde no hay interés comercial. Así surgieron
proyectos como [guifi.net](https://guifi.net) en Catalunya, Delta
y Quintana Libres en Argentina, además de las redes inalámbricas
con acceso comunitario a internet, o la red de telefonía celular
comunitaria en Oaxaca, México, como [Rhizomatica](https://www.rhizomatica.org).

Estas redes son gestionadas por sus comunidades, que toman las
decisiones en relación con la infraestructura de red: es construida
por ellas mismas. _Estas son las redes que queremos, más que
neutras: libres y comunitarias_. Que no te engañen, la infraestructura
de redes autónomas está al alcance, son las legislaciones las
que las imposibilitan.

</section>
<section epub:type="chapter" role="doc-chapter">

# Situación en Argentina

En 2014 se aprobó la [ley 27.078 «Argentina Digital»](http://servicios.infoleg.gob.ar/infolegInternet/anexos/235000-239999/239771/norma.htm).
Entre otras cosas, garantiza la neutralidad de red en su artículo
1 y la define en el 56 y 57. Esta ley fue [reglamentada en 2016](http://www.saij.gob.ar/1340-nacional-reglamentacion-leyes-nros-26522-27078-sobre-servicios-comunicacion-audiovisual-tecnologias-informacion-comunicaciones-dn20160001340-2016-12-30/123456789-0abc-043-1000-6102soterced).

Las personas que integran al Partido Interdimensional Pirata
_participamos en las sesiones abiertas en las que se definió
esta ley_ y propusimos junto con [Altermundi](http://altermundi.net/)
la defensa de las redes comunitarias y [otras propuestas](https://wiki.partidopirata.com.ar/images/d/d0/Argentina-digital.pdf)
que no fueron incluidas, como la financiación de las redes comunitarias.

Sin embargo, si bien se observa un espíritu democratizador en
el que el Estado se sitúa como el garante del «acceso universal»
a las nuevas tecnologías ---y los mecanismos que las hacen posibles---
o como el regulador del mercado, esta normativa fue sancionada
y promulgada hace tres años.

En la actualidad, el ejecutivo, vía una mesa redactora, procura
darle forma al anteproyecto de _Ley de Comunicaciones Convergentes_.
Este proyecto todavía no llega al congreso, pero sus 17 puntos
iniciales publicados por el Ente Nacional de Comunicaciones (+++ENACOM+++)
en julio del 2016 [generaron reacciones en contra](https://cpr.org.ar/article/a-que-llama-el-gobierno-comunicaciones-convergente/).

Según nuestra interpretación de aquellos 17 puntos basales, consideramos
que chocan con lo previsto en el último párrafo del artículo
2 de la ley 27.078 donde se afirma:

> Asimismo, se busca establecer con claridad la distinción entre
> los mercados de generación de contenidos y de transporte y distribución
> de manera que la influencia en uno de esos mercados no genere
> prácticas que impliquen distorsiones en el otro.

Bajo la fachada de generar «[plena libertad de expresión y prensa,
visibles mejoras en materia tecnológica y nuevas inversiones
en el país](https://www.enacom.gob.ar/institucional/la-comision-redactora-presento-los-17-principios-de-la-nueva-ley_n1271)»,
la normativa que reemplazará a la ley 26.522 de Servicios de
Comunicación Audiovisual (+++LSCA+++). Esto favorecerá la concentración,
ya que se permitirá la adjudicación de licencias a un mismo prestador
para que brinde múltiples servicios ---señal de cable, telefonía,
proveedores de acceso a internet (+++PAI+++ o +++ISP+++ por sus
siglas en inglés) con medios de comunicación, entre otros---.

Al priorizar el negocio con las telecomunicaciones, no solo se
estaría ante una medida regresiva, sino que se trataría de una
clara situación de desventaja a favor de quienes manejarán el
acceso a internet. Es decir, el internet será plenamente privatizado.

A principios del 2018, Andrés Gil Domínguez, integrante de la
comisión de redacción, publicó en su _blog_ personal el documento
preliminar del [anteproyecto de _Ley de Comunicaciones Convergentes_](http://www.anred.org/spip.php?article15865).
Cuenta con 63 páginas y 196 artículos. Allí observamos que el
capítulo IV del título V está dedicado enteramente a definir
la neutralidad de red, aunque _no aplica multas ni sanciones_
como en otras fracciones. También establece la inviolabilidad
de las comunicaciones salvo pedido de un juez, a la vez que establece
la conservación de datos y registros por al menos 3 años ---véase
artículo 155---. Como quien dice, da una de cal y otra de arena.

¡Encima se olvidaron del artículo sobre el fomento a las redes
comunitarias!

Si bien se trata de un documento preliminar, que un integrante
de una mesa chica de redacción publique en su _blog_ personal
---pese a los reiterados pedidos del Centro de Producciones Radiofónicas
de tenerlo en mano y aunque podría no ser el documento definitivo
debido a sendos pedidos de prórroga al Congreso--- nos alarma
que empiece a ser tomado en cuenta pese a que ya existe una normativa
reguladora de la neutralidad de red. Sin dudas, que una de las
prestadoras de telefonía celular privilegie el tráfico de WhatsApp,
Messenger, Twitter o Instagram ---en detrimento de otras aplicaciones---
nos pone de manifiesto que la administración actual del Estado
argentino no le importa mantener la neutralidad ni promover las
redes libres y comunitarias gestionadas por sus usuarias.

</section>
<section epub:type="chapter" role="doc-chapter">

# Situación en México

En 2014 y a partir de la reforma a los artículos 145 y 146 de
la Ley Federal de Telecomunicaciones y Radiodifusión se indica
que el Instituto Federal de Telecomunicaciones (+++IFT+++) debe
emitir lineamientos para la protección de la neutralidad de red.
Estos deben observar principios como los de libre elección, no
discriminación, privacidad y transparencia.

Sin embargo, en la práctica la neutralidad de red está desprotegida
en México. El +++IFT+++ no emitió ningún lineamiento durante
más de cinco años. Hasta diciembre del 2019 este instituto publicó
el [_Anteproyecto de Lineamientos sobre Gestión de Tráfico_](http://www.ift.org.mx/sites/default/files/industria/temasrelevantes/13791/documentos/1documentoenconsultapublicaanteproyectodelineamientos_0.pdf),
el cual pretende cumplir con la obligación que establece la ley.
A partir de esa fecha y hasta el 13 de abril del 2020 el anteproyecto
está sometido a consulta pública.

Salvemos Internet ---una coalición formada por organizaciones,
empresas, colectivos y personas interesadas en la defensa de
la neutralidad de red--- y demás entidades opinamos que los lineamientos
propuestos por el +++IFT+++ amenazan gravemente la neutralidad
de red, la privacidad, la libertad y la apertura en internet.
Existen al menos cuatro grandes amenazas:

a. _Censura_. Los lineamientos abren la puerta para el bloqueo
gubernamental de aplicaciones, contenidos y servicios (+++ACS+++)
por «situaciones de emergencia y seguridad nacional» o «a petición
expresa de autoridad competente» pese a que la Constitución mexicana
lo prohíbe.

b. _Priorización pagada_. Los lineamientos permitirían a los
+++PAI+++ llegar a acuerdos comerciales para favorecer el tráfico
de sus socios o para dar un trato preferencial a sus +++ACS+++.
De esta manera, los +++PAI+++ se convertirían en «cadeneros»
del internet, ya que favorecerían o perjudicarían según sus intereses
comerciales. De quedarse el anteproyecto así, tendríamos como
consecuencia la alza de precios y de impedimentos para la operación
de nuevas +++ACS+++. Es decir, la priorización pagada implica
una menor diversidad y pluralidad en internet.

c. _Invasión a la privacidad_. La priorización de ciertas +++ACS+++
implica la discriminación del tráfico en internet. Para ello,
los +++PAI+++ tendrían que monitorear el tráfico mediante técnicas
como la inspección profunda de paquetes, la cual también puede
ser aplicada en denuesto a la privacidad de las personas usuarias
de internet. A pesar de que la ley obliga el respeto al derecho
a la privacidad, los lineamientos omiten cualquier protección
al respecto.

d. _Transparencia insuficiente_. Los lineamientos no contemplan
medidas adecuadas para el monitoreo transparente, lo que impide
evaluar el cumplimiento a la neutralidad de red. La ausencia
de medidas efectivas hace imposible la rendición de cuentas,
así como fomenta la impunidad ante las violaciones a la neutralidad
de red y a la privacidad en internet.

La neutralidad de red implica que los +++PAI+++ no pueden bloquear,
degradar, perjudicar o favorecer ciertas +++ACS+++. Para fomentarla
se requiere que expreses tu descontento ante los lineamientos
propuestos por el anteproyecto publicado por la +++IFT+++. Algunas
acciones que puedes llevar a cabo son:

Haz llegar tu opinión sobre el anteproyecto. Existen dos maneras
sencillas de llevarlo a cabo: puedes llenar [el formulario](https://salvemosinternet.mx/#tab-container)
disponible en [salvemosinternet.mx](https://salvemosinternet.mx);
si tienes cuenta en Twitter, solo basta con que des +++RT+++
al [_tweet_ fijado](https://twitter.com/BotSalvemos/status/1236458856127123456)
de [\@BotSalvemos](https://twitter.com/BotSalvemos).

Producir videos, gráficos, _podcast_, memes o publicaciones.
Con esto contribuyes a la sensibilización de más personas sobre
la importancia de la neutralidad de red ---como pretende este
_fanzine_---.

Organizar eventos y talleres. Una parte central para el fomento
de un internet libre y abierto es la difusión y la enseñanza
de conocimientos. El internet es también un espacio político
en el que cualquier persona puede contribuir en su gestación.
Por ejemplo, la muestra documental de [salvemosinternet.tk](https://salvemosinternet.tk)
es un evento pensado para dar a conocer qué es la neutralidad
de red y cómo en América Latina se está trabajando para el desarrollo
de redes autónomas.

Experimenta y genera alternativas comunicativas. En la actualidad
y de manera global existe la inclinación por constituir legislaciones
que no favorecen los derechos de las personas usuarias de internet.
El caso mexicano y argentino son unos ejemplos. El empuje y la
reelaboración constante de legislaciones en pos de los intereses
de los +++PAI+++ son la evidencia de que otra manera de comunicarnos
no solo es posible, sino también necesaria. A la par de la exigencia
de leyes más transparentes y a favor de las personas, prueba
con diversas tecnologías que poco a poco te hagan menos dependiente
de las +++ACS+++ que los +++PAI+++ te ofrecen o dan preferencia.
Con ello contribuyes a la diversificación del internet al mismo
tiempo que poco a poco detienes la intromisión de agentes externos
en tu manera de habitar en línea.

Construye comunidad. Con amistades, familiares, _hacklabs_ y
demás organizaciones en pos de los derechos humanos ---como es
el acceso a la información y la comunicación--- puedes empezar
a cimentar grupos de trabajo. La legislación del internet tiende
a un reduccionismo jurídico y técnico en el que al parecer son
pocas las personas aptas para la emisión de recomendaciones y
la constitución de lineamientos. No obstante, para varias personas
el internet también es un lugar donde de manera mutua nos damos
cuidados, por lo que es necesario que velemos por su protección.
Luchemos en conjunto en pos de un internet multifacético y diverso:
evitemos que su rumbo quede en manos de pocos individuos y entidades.

</section>
