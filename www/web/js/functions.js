let scroll_position = [0, 0];

// Modifica el comportamiento del scroll
function init_scroll () {
  var btns = document.getElementsByClassName('nav-btn');

  // Iteración de cada botón
  for (var i = 0; i < btns.length; i++) {
    var btn = btns[i];

    // Añade un escucha
    btn.addEventListener('click', function (e) {
      var a    = e.target.nodeName == 'A' ? e.target : e.target.parentNode,
          id   = a.href.split('#')[a.href.split('#').length - 1],
          div  = document.getElementById(id);

      // Previene comportamiento por defecto
      e.preventDefault();

      // Mueve la vista al elemento indicado
      div.scrollIntoView({
        behavior: 'smooth'
      });   
    });
  }
}

window.onload = function () {
  init_scroll();
} 
